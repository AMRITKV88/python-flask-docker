'''
Created on Nov 12, 2019

@author: gelblars
'''

import unittest
from app import core


class BasicTestSuite(unittest.TestCase):

    def test_sample_func(self):
        user = core.get_user()
        self.assertIn('username', user, "Missing username in user object")
        self.assertIn('email', user, "Missing email in user object")

if __name__ == '__main__':
    unittest.main()
