FROM python:3.7-alpine

LABEL maintainer="your@email.com"

ENV PYTHONPATH="/"

ADD ./requirements /requirements/

RUN pip install --no-cache-dir -r requirements/req.txt

ADD ./app /app/

CMD [ "python", "./app/core.py" ]