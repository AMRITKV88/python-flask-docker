function download_chart() {
  add_chart_repositories

  if [[ -d chart ]]; then
    echo "Download skipped. Using the chart at local path 'chart/'..."
  elif [[ -n "$AUTO_DEVOPS_CHART" ]]; then
    # user specified a custom chart to use, but it can be a local directory or a remote chart
    if [[ -d "$AUTO_DEVOPS_CHART" ]]; then
      echo "Download skipped. Using the chart at local path '$AUTO_DEVOPS_CHART' (moving to 'chart/' first)..."
      mv "$AUTO_DEVOPS_CHART" chart/
    else
      echo "Downloading remote chart '$AUTO_DEVOPS_CHART'..."
      helm pull "$AUTO_DEVOPS_CHART" --untar

      local auto_chart_name
      auto_chart_name=$(basename "$AUTO_DEVOPS_CHART")
      auto_chart_name=${auto_chart_name%.tgz}
      auto_chart_name=${auto_chart_name%.tar.gz}
      if [[ "$auto_chart_name" != "chart" ]]; then
        mv "$auto_chart_name" chart
      fi
    fi
  else
    echo "Download skipped. Using the default chart included in auto-deploy-image..."
    cp -R "$ASSETS_CHART_DIR" chart
  fi

  if [[ -f chart/requirements.lock ]]; then
    helm dependency build chart/
  else
    helm dependency update chart/
  fi

  helm install example ./chart

}

download_chart

