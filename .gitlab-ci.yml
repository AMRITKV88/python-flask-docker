#### Gitlab-CI template
#### This template enables you to test, build, upload, deploy and release your application
#### By default test build and upload to local docker registry is enabled.
#### To enable this pipeline the following variables must be set in Settings->CI/CD->Variables

image: docker.artifactory.int.idevops.de/mydevops/python-build:latest

variables:
  APP_NAME: myapp
  #Make sure to enable the docker registry via:  Settings > General > Visibility, project features, permissions
  DOCKER_REGISTRY: docker.siemens.com

stages:
  - test
  - build
  - upload
  - deploy
  - release

.common-dind:
  services:
    - docker:18.09-dind
  tags:
    - idevops
    - dind
  variables:
    DOCKER_HOST: tcp://localhost:2375/

##### TESTS #####
.common-test:
  stage: test
  variables:
    PYTHONPATH: ${CI_PROJECT_DIR}
  dependencies: []
  tags:
    - idevops

.common-test-build:
  extends: .common-test
  script:
    - tox -e setup

.common-test-unit:
  extends: .common-test
  script:
    - tox -e py37
  artifacts:
    reports:
      junit: build/junit-*.xml
    untracked: false
    expire_in: 1 day
    paths:
      - build/junit-*.xml
    when: always

.common-test-coverage:
  extends: .common-test
  script:
    - tox -e coverage
  artifacts:
    untracked: false
    expire_in: 1 day
    paths:
      - build/coverage/
    when: always

.common-test-lint:
  extends: .common-test
  script:
    - tox -e lint
  artifacts:
    untracked: false
    expire_in: 1 day
    paths:
      - pylint*.xml
    when: always

.common-test-docs:
  extends: .common-test
  script:
    - tox -e docs
  artifacts:
    untracked: false
    expire_in: 1 day
    paths:
      - build/sphinx/html/

test:build:
  extends: .common-test-build

test:unit-test:
  extends: .common-test-unit

test:coverage:
  extends: .common-test-coverage

test:lint:
  extends: .common-test-lint

test:docs:
  extends: .common-test-docs

##### BUILD #####
build:
  extends: .common-dind
  stage: build
  script:
    - docker build -t ${APP_NAME} .
    - docker images
    - mkdir images
    - docker save ${APP_NAME} > images/${APP_NAME}.tar
  dependencies: []
  artifacts:
    untracked: false
    expire_in: 1 hour
    paths:
      - images

##### UPLOAD #####
.common-docker-push:
  extends: .common-dind
  stage: upload
  script:
    - echo ${DOCKER_TAG}
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN ${DOCKER_REGISTRY}
    - docker load -i images/${APP_NAME}.tar
    - docker tag ${APP_NAME}:latest ${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/${APP_NAME}:${DOCKER_TAG}
    - docker push ${DOCKER_REGISTRY}/${CI_PROJECT_PATH}/${APP_NAME}:${DOCKER_TAG}
    - echo export DOCKER_TAG=${DOCKER_TAG} >> images/env_variables.var
  dependencies:
    - build

push:tagged:
  extends: .common-docker-push
  before_script:
    - DOCKER_TAG=$(git describe --always --tags --long)-${CI_COMMIT_REF_SLUG}
  variables:
    DOCKER_TAG: ${DOCKER_TAG}
  artifacts:
    untracked: false
    expire_in: 1 hour
    paths:
      - images/env_variables.var

push:release:
  extends: .common-docker-push
  only:
    refs:
      - tags
  variables:
    DOCKER_TAG: ${CI_COMMIT_TAG}

push:latest:
  extends: .common-docker-push
  only:
    refs:
      - master
  variables:
    DOCKER_TAG: latest
    
##### DEPLOY #####
##### To enable deploy set $DEPLOY_PIPELINE_URL and $DEPLOY_PIPELINE_TOKEN
.common-trigger-deploy:
  stage: deploy
  script:
    - echo ${STAGE}
    - echo ${DOCKER_TAG}
    - >
      curl --insecure --request POST
      --form "variables[STAGE]=${STAGE}"
      --form "variables[DOCKER_TAG]=${DOCKER_TAG}"
      --form "token=${DEPLOY_PIPELINE_TOKEN}"
      --form ref=master
      ${DEPLOY_PIPELINE_URL}
  only:
    refs:
      - tags
      - master
    variables:
      - $DEPLOY_PIPELINE_URL
      - $DEPLOY_PIPELINE_TOKEN
  dependencies:
    - push:tagged

trigger:release:
  extends: .common-trigger-deploy
  only:
    refs:
      - tags
  variables:
    STAGE: production
    DOCKER_TAG: ${CI_COMMIT_TAG}
    
trigger:staging:
  extends: .common-trigger-deploy
  before_script:
    - source images/env_variables.var
  only:
    refs:
      - master
  variables:
    STAGE: testing
    DOCKER_TAG: ${DOCKER_TAG}
    
##### RELEASE #####
##### To enable release feature please provide your personal token as $RELEASE_TOKEN
release:
  stage: release
  script:
    - echo ${CI_COMMIT_TAG}
    - >
      curl --header 'Content-Type: application/json'
      --header "PRIVATE-TOKEN: ${RELEASE_TOKEN}"
      --data "{\"name\": \"Release ${CI_COMMIT_TAG}\", \"tag_name\": \"${CI_COMMIT_TAG}\", \"description\": \"Released vi CI/CD\", \"assets\": { \"links\": [{ \"name\": \"DockerImage\", \"url\": \"https://artifactory.int.idevops.de/artifactory/webapp/#/artifacts/browse/tree/General/docker/eaas/eaas\" }] } }"
      --request POST
      https://code.siemens.com/api/v4/projects/${CI_PROJECT_ID}/releases
  only:
    refs:
      - tags
    variables:
      - $RELEASE_TOKEN
