# python-flask-demo
This repository includes everything to get started with python-flask.
Beside the basic "Hello-World" code sample this repository also provides a pre-prepared DevOps pipeline based on gitlab-ci.

#### Repository Structure
The basic folder/file structure of the repository looks like:

* **app** includes source code of your application
* **docs** includes basic files to automatically create a python documentation. Further this folder can used to store additional documents or pictures for your project documentation
* **requierments** holds a simple req.txt file with all python packages requiered to run your application. This file is used during the docker container creation.
* **tests** includes all unittests
* **.gitlab-ci.yml** is the gitlab-ci pipeline file.
* **Dockerfile** is the Dockerfile used to create your rdy-to-run docker image

#### DevOps Pipeline
The pre-configurated pipeline follows the [Github flow](https://guides.github.com/introduction/flow/) pattern. Basically, only owner and maintainer are able to commit
to the master branch. Developers should just create a personal/feature-branch and open a Merge-Request as soon they think the code is ready for review.

The pipeline provides five main stages with several sub-stages:
* **test** the test stage runs unittests, provides a lint and coverage reports and creates a python doc.
* **build** the build stages creates the docker container
* **upload** the upload stage uploads the docker container into the local docker.siemens.com repository
* **deploy** (deactivated by default) this stage can trigger an additional pipeline which handle the deploy into production and/or testing stages. In order to activate this feature the $DEPLOY_PIPELINE_URL and $DEPLOY_PIPELINE_TOKEN must set in the Settings->CI/CD->Variables. To get more input about the staged-pipeline pattern please check the [Environment Orchestration Paper](https://software-foundation.siemens.com/deliverable/EnvironmentOrchestration).
* **release** Creates a release for your application in the [Release-Page](https://code.siemens.com/%{project_path}/-/releases) this can be triggered by just creating a new tag like 0.0.1 on the [Tag-Page](https://code.siemens.com/%{project_path}/-/tags).

The concept of the pipeline is sketched in the following picture:
![](./docs/pipeline-demo.png)

