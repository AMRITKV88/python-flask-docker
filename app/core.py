"""
Created on Oct 21, 2019

@author: gelblars
"""
from flask import Flask, render_template
from app.config.config import AppConfig


app = Flask(__name__)
app.config.from_object(AppConfig)

def get_user():
    user = {
        'username': "Unkown",
        'email': 'mail@mir.net'
    }
    return user

@app.route('/')
@app.route('/index')
def index():
    user = get_user()
    return render_template('index.html', user=user, title='Home')

if __name__ == "__main__":
    app.run(host='0.0.0.0')